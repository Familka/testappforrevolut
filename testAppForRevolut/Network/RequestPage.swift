//
//  MoyaProvider.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 12/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Moya

public enum RequestPage {
    case rates(String?)

    public var baseURL: URL {
        return URL(string: "https://revolut.duckdns.org")!
    }
}

extension RequestPage: TargetType {
    public var path: String {
        switch self {
        case .rates: return "latest"
        }
    }

    public var method: Method {
        switch self {
        case .rates: return .get
        }
    }

    public var task: Task {
        switch self {
        case let .rates(rates):
            var params: [String: Any] = [:]
            params["base"] = rates
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }

    public var sampleData: Data { return Data() }

    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
}
