//
//  RatesModel.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 20/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Foundation
import Moya

protocol RatesViewModelDelegate: class {
    func reloadTable()
}

final class RatesViewModel {
    private let serviceLayer: CurrencyServiceLayerProtocol

    weak var delegate: RatesViewModelDelegate?

    private(set) var ratess = [Rate]()

    private var currentRequest: Cancellable?
    var baseRate = Rate(name: "USD", baseRate: 1)
    var timer = Timer()

    init(serviceLayer: CurrencyServiceLayerProtocol = CurrencyServiceLayer()) {
        self.serviceLayer = serviceLayer
    }

    func getRates(rate: String?) {
        currentRequest = serviceLayer.getRates(rate: rate) { [weak self] result in
            guard let `self` = self else { return }
            self.ratess = result.rates.map { Rate(name: $0, baseRate: $1) }
            self.ratess.insert(self.baseRate, at: 0)
            self.delegate?.reloadTable()
        }
    }

    func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            guard let `self` = self else { return }
            self.getRates(rate: self.baseRate.name)
        }
    }
}
