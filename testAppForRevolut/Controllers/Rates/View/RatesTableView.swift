//
//  RatesView.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 20/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit

protocol RatesViewDataSource: class {
    func numberOfRows() -> Int
    func cellForRow(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
    func didSelectRowAt(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
}

final class RatesTableView: UIView {
    weak var dataSource: RatesViewDataSource?
    weak var cellDelagete: RatesTableViewCell?

    let tableView: UITableView = .make {
        $0.tableFooterView = UIView()
        $0.backgroundColor = UIColor.orange
        $0.register(RatesTableViewCell.self,
                    forCellReuseIdentifier: RatesTableViewCell.reuseIdentifier)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        makeConstraints()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let margins = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        tableView.frame = frame.inset(by: margins)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RatesTableView {
    private func makeConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.snp.makeConstraints { make in
            make.left.equalTo(snp.left)
            make.top.equalTo(snp.top)
            make.bottom.equalTo(snp.bottom)
        }
    }
}

extension RatesTableView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return dataSource?.numberOfRows() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return dataSource?.cellForRow(tableView: tableView, indexPath: indexPath) ?? UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return (dataSource?.didSelectRowAt(_: tableView, didSelectRowAt: indexPath))!
    }
}
