//
//  RatesTableViewCell.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 12/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import SnapKit
import UIKit

protocol RatesTableViewCellDelegate: NSObjectProtocol {
    func ratesTableViewCellStart(_ cell: RatesTableViewCell)
    func ratesTableViewCellEnd(_ cell: RatesTableViewCell)
    func ratesTableViewCellEditing(_ cell: RatesTableViewCell, text: String?)
}

final class RatesTableViewCell: UITableViewCell {
    private let rateName = UILabel()
    private let rateValue: UITextField = .make {
        $0.keyboardType = .decimalPad
        $0.textAlignment = .right
    }

    weak var delegate: RatesTableViewCellDelegate?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        rateValue.delegate = self
        addSubview(rateName)
        addSubview(rateValue)
        makeConstraint()
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Конфигуратор ячейки
    ///
    /// - Parameter rates: Валюта(Название, текущее значение)
    func configure(_ rate: Rate) {
        rateName.text = rate.name
        rateValue.text = rate.resultRate
    }

    func makeConstraint() {
        rateName.snp.makeConstraints { make in
            make.leading.equalTo(snp.leadingMargin)
            make.centerY.equalToSuperview()
        }

        rateValue.snp.makeConstraints { make in
            make.trailing.equalTo(snp.trailingMargin)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
        }
    }
}

extension RatesTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            delegate?.ratesTableViewCellEditing(self, text: updatedText)
        }
        return true
    }

    func textFieldDidBeginEditing(_: UITextField) {
        delegate?.ratesTableViewCellStart(self)
    }

    func textFieldDidEndEditing(_: UITextField) {
        delegate?.ratesTableViewCellEnd(self)
    }
}
