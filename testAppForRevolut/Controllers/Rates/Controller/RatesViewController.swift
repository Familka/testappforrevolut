//
//  RatesTableViewController.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 12/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit

final class RatesViewController: UIViewController {
    private let ratesTableView = RatesTableView()
    private let viewModel: RatesViewModel

    init(viewModel: RatesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        title = "Валюты"
        view = ratesTableView
        ratesTableView.dataSource = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getRates(rate: nil)
        viewModel.startTimer()
    }
}

extension RatesViewController: RatesViewModelDelegate {
    func reloadTable() {
        ratesTableView.tableView.reloadData()
    }
}

extension RatesViewController: RatesTableViewCellDelegate {
    func ratesTableViewCellStart(_: RatesTableViewCell) {
        viewModel.timer.invalidate()
    }

    func ratesTableViewCellEnd(_: RatesTableViewCell) {
        viewModel.startTimer()
    }

    func ratesTableViewCellEditing(_: RatesTableViewCell, text: String?) {
        let coef = Decimal(string: text ?? "0")
        var updateIndex = [IndexPath]()
        for i in 1 ..< viewModel.ratess.count {
            viewModel.ratess[i].resultRate = "\(viewModel.ratess[i].baseRate * (coef ?? 0))"
            updateIndex.append(IndexPath(row: i, section: 0))
        }
        ratesTableView.tableView.reloadRows(at: updateIndex, with: .automatic)
    }
}

extension RatesViewController: RatesViewDataSource {
    func didSelectRowAt(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.baseRate = viewModel.ratess[indexPath.row]
        reloadTable()
    }

    func numberOfRows() -> Int {
        return viewModel.ratess.count
    }

    func cellForRow(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: RatesTableViewCell.reuseIdentifier,
            for: indexPath) as! RatesTableViewCell
        cell.configure(viewModel.ratess[indexPath.row])
        cell.delegate = self
        return cell
    }
}
