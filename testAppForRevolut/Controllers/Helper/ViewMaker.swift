//
//  ViewMaker.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 25/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit

extension UIView {
    static func make<T: UIView>(config: (T) -> Void) -> T {
        let view = T()
        config(view)
        return view
    }
}
