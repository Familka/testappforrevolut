//
//  NavigationViewController.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 27/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit

/// Основной `NavigationController`
final class NavigationController: UINavigationController {
    
    override init(rootViewController: UIViewController) {
        super.init(nibName: nil, bundle: nil)
        navigationBar.tintColor = UIColor.white
        navigationBar.barTintColor = UIColor.orange
        navigationBar.prefersLargeTitles = true
        navigationBar.largeTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 30)
        ]
       pushViewController(rootViewController, animated: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
