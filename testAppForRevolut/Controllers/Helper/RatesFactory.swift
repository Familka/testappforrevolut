//
//  RatesFactory.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 20/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit

final class RatesFactory {
    
    func create() -> UIViewController {
        let viewModel = RatesViewModel()
        let ratesViewController = RatesViewController(viewModel: viewModel)
        viewModel.delegate = ratesViewController
        return ratesViewController
    }
}
