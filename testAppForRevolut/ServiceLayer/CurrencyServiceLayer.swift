//
//  RateServiceLayer.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 12/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Foundation
import Moya

typealias Handler<T> = (T) -> Void

protocol CurrencyServiceLayerProtocol {
    func getRates(rate name: String?,
                  completion: @escaping Handler<Currency>) -> Cancellable
}

struct CurrencyServiceLayer: CurrencyServiceLayerProtocol {
    /// Доступ к запросам
    private let provider = MoyaProvider<RequestPage>()

    /// Запрос для получения курса валют относительно текущей валюты
    ///
    /// - Parameters:
    ///   - name: название валюты, относительно какой получить курс
    ///   - completion: Хелпер подкачки
    func getRates(rate name: String?,
                  completion: @escaping Handler<Currency>) -> Cancellable {
        return provider.request(.rates(name)) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(Currency.self, from: response.data)
                    completion(results)
                } catch {
                    print("Parsing Error")
                }

            case let .failure(error):
                print(error)
            }
        }
    }
}
