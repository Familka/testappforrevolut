//
//  Rate.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 19/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Foundation

/// Валюта
final class Rate {
    /// Название валюты
    let name: String

    /// Значение валюты
    var baseRate: Decimal

    /// Результат
    var resultRate: String

    init(name: String, baseRate: Decimal) {
        self.name = name
        self.baseRate = baseRate
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        resultRate = formatter.string(from: baseRate as NSNumber)!
    }
}
