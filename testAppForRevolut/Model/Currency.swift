//
//  Rates.swift
//  testAppForRevolut
//
//  Created by Фамил Гаджиев on 12/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Foundation

/// Базовая модель
struct Currency: Codable {
    let base: String
    let date: String
    let rates: [String: Decimal]
}
