//
//  Fixtures.swift
//  testAppForRevolutTests
//
//  Created by Фамил Гаджиев on 27/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Foundation

/// Тип который может создавать Fixtures.
protocol Fixturаble {
    
    /// Бандл из которого будут браться ресурсы для создания Fixture.
    var bundle: Bundle { get }
}
