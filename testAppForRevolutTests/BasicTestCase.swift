//
//  BasicTestCase.swift
//  testAppForRevolutTests
//
//  Created by Фамил Гаджиев on 27/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Foundation

/// Базовый класс для тестирования.
class BasicTestCase: XCTestCase, Fixturаble {
    
    /// `Bundle` тестового таргета.
    ///
    /// - Note: Используйте для получения ресурсов из тестового таргета.
    /// - SeeAlso: `Bundle+Test.swift`.
    var bundle: Bundle { return Bundle(for: BasicTestCase.self) }
}
